import { normalizeData } from './../utils/normalizeData';
import { IVacancyResponse, INormalizeData } from './requests.interface';
import axios from 'axios';

const url = 'https://api.hh.ru/'

export function getVacanciesReq(page: number) {
    return new Promise<INormalizeData[]>((resolve, reject) => {
        return axios.get<IVacancyResponse>(`${url}vacancies?page=${page}&per_page=20`).then((res) => {    
            const vacancies: INormalizeData[] = normalizeData(res.data.items)  
            if(vacancies) {
                resolve(vacancies);
            }    
        });
        
    })
}
