import { IVacancy, INormalaizeSalary } from "./../reducers/state.interface";

export interface IVacancyResponse {
    items: IVacancy[];
}

export interface INormalizeData {
    id: string;
    name: string;
    location: string | null
    vacancyUrl: string;
    salary: INormalaizeSalary | null;
    employer: string;
    requirement: string | null;
}
