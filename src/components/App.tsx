import * as React from "react";
import { connect } from "react-redux";

import { IAppState } from "../reducers/state.interface";

import "../styles/main.scss";
import { INormalizeData } from "../requests/requests.interface";
import { getVacanciesAction } from "../actions/actions"
import Table from "./table/Table";
import Filter from "./filter/Filter";
import Button from "../commons/Button";

export interface IAppProps {
    data: INormalizeData[];
    getVacanciesAction: () => void
}

class App extends React.Component<IAppProps> {

    onClickLoadVacancies = () => {
        this.props.getVacanciesAction()
    }
    
    public render() {
        return (
            <div className="container wrapper">
                {this.props.data.length > 0 && (
                    <>
                        <Filter />
                        <Table />
                    </>
                )}
                <Button buttonName="LOAD VACANCIES" onClick={this.onClickLoadVacancies} />
            </div>
        );
    }
}

const mapStateToProps = (state: IAppState) => {
    return {
        data: state.data,
    };
};
const mapDispatchToProps = {
    getVacanciesAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
