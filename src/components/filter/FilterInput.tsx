import * as React from "react";

export interface IFilterInputProps {
    filterName: string;
    onChange: (value: string, prop: string) => void;
    keyNumber: string;
}

export default class FilterInput extends React.Component<IFilterInputProps> {
    state = {
        value: "",
    };
    onChangeSaveInputValue = (event: React.ChangeEvent<HTMLInputElement>) => {
        const value = event.currentTarget.value;
        const prop = event.currentTarget.id;
        this.setState(
            {
                value,
            },
            () => {
                this.props.onChange(value, prop);
            }
        );
    };
    public render() {
        const { filterName } = this.props;
        const groupId = "basic-addon" + this.props.keyNumber;
        return (
            <div className="input-group filter">
                <div className="input-group-prepend">
                    <span className="input-group-text" id={groupId}>
                        {`By ${filterName}`}
                    </span>
                </div>
                <input
                    type="text"
                    className="form-control"
                    placeholder="Enter your filter"
                    aria-describedby={groupId}
                    onChange={this.onChangeSaveInputValue}
                    id={filterName}
                    value={this.state.value}
                ></input>
                <div className="input-group-append" tabIndex={-1}>
                    <button className="btn btn-outline-secondary" type="button" tabIndex={-1}>
                        x
                    </button>
                </div>
            </div>
        );
    }
}
