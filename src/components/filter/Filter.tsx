import * as React from "react";
import FilterInput from "./FilterInput";
import { connect } from "react-redux";
import { filterDataAction } from "../../actions/actions";

const filterNames = {
    name: "Vacancy Name",
    location: "Location",
    employer: "Employer",
    salary: "Salary",
    requirement: "Reuirements",
};

export interface IFilterProps {
    filterDataAction: Function
}

class Filter extends React.Component<IFilterProps> {
    
    filterData = (value: string, prop: string) => {
        this.props.filterDataAction(value, prop)
    }
    renderFilterInputs = () => {
        const names = Object.keys(filterNames);
        return names.map((name, keyNumber) => {
            return (
                <FilterInput
                    keyNumber={name + keyNumber}
                    filterName={name}
                    onChange={this.filterData}
                    key={name}
                />
            );
        });
    };

    public render() {
        return <div>{this.renderFilterInputs()}</div>;
    }
}


const mapDispatchToProps = {
    filterDataAction,
};

export default connect(null, mapDispatchToProps)(Filter);
