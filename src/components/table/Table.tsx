import * as React from "react";
import { connect } from "react-redux";
import { IAppState } from "../../reducers/state.interface";
import { INormalizeData } from "../../requests/requests.interface";
import { getVacanciesAction, modifyRenderData } from "../../actions/actions";
import TableRow from "./TableRow";
import Paging from "./pagination/Paging";

export interface ITableProps {
    renderData: INormalizeData[];
    totalAmountPages: number;
    currentPage: number;
    getVacanciesAction: (pageNumber?: number) => void;
    modifyRenderData: (newRenderData: INormalizeData[]) => void
}

class Table extends React.Component<ITableProps> {
    state = {
        totalAmountPages: this.props.totalAmountPages,
        renderData: this.props.renderData,
        currentPage: this.props.currentPage
    };

    renderRows = () => {
        return this.props.renderData.map((vacancy, index) => {
            return (
                <TableRow
                    key={vacancy.id}
                    vacancy={vacancy}
                    index={index}
                    onDrop={this.dragDrop}
                />
            );
        });
    };


  
    dragDrop = (id: string, isDraggingIndex: number) => {    
        let renderData = this.props.renderData;
        const draggingData = renderData.splice(isDraggingIndex, 1)[0];
        renderData.splice(parseInt(id), 0, draggingData);
        this.props.modifyRenderData(renderData)
    };


    getNewPage = (event: React.SyntheticEvent<HTMLButtonElement>) => {
        const id = event.currentTarget.id;
        let pageNumber: number = 0;
        if (id === "previouse") {
            pageNumber = this.state.currentPage - 1;
        } else if (id === "next") {
            pageNumber = this.state.currentPage + 1;
        } else {
            pageNumber = parseInt(id);
        }
        this.setState({
            currentPage: pageNumber,
        });
        this.props.getVacanciesAction(pageNumber);
    };

    // componentWillReceiveProps(nextProps: ITableProps) {
    //     console.log(nextProps);
    // }


    public render() {       
        const { totalAmountPages, currentPage, renderData } = this.state;
        return (
            <div>
                <table className="table ">
                    <tbody>
                        <tr className="table__row">
                            <th className="table__column title">
                                Vacancy Name
                            </th>
                            <th className="table__column title">Location</th>
                            <th className="table__column title">Employer</th>
                            <th className="table__column title">
                                Salary (CUR)
                            </th>
                            <th className="table__column title">
                                Requirements
                            </th>
                        </tr>
                        {renderData.length > 0 && this.renderRows()}
                    </tbody>
                </table>
                <Paging
                    totalAmountPages={totalAmountPages}
                    getNewPage={this.getNewPage}
                    currentPage={currentPage}
                />
            </div>
        );
    }
}

const mapStateToProps = (state: IAppState) => {
    return {
        renderData: state.renderData,
        totalAmountPages: state.totalAmountPages,
        currentPage: state.currentPage,
    };
};

const mapDispatchToProps = {
    getVacanciesAction,
    modifyRenderData
};

export default connect(mapStateToProps, mapDispatchToProps)(Table);
