import * as React from "react";
import { INormalizeData } from "../../requests/requests.interface";

export interface ITableRowProps {
    vacancy: INormalizeData;
    index: number;
    onDrop: (id: string, isDraggingIndex: number) => void;
}

export default class TableRow extends React.Component<ITableRowProps> {
    state = {
        isDragOver: false,
        isDragging: false,
    };
    setDragIndex = (index: number) => {
        this.setState({
            isDraggingIndex: index
        })
    }
    onDragStart = (event: React.DragEvent<HTMLTableRowElement>) => {
        const id = event.currentTarget.id;
        event.dataTransfer.setData('text', id)
      
        setTimeout(() => {            
            this.setState({
                isDragging: true,
            });
        }, 0);
    };
    onDragLeave = (event: React.DragEvent<HTMLTableRowElement>) => {
        event.stopPropagation();
        if (this.state.isDragOver) {
            this.setState({
                isDragOver: false,
            });
        }
    };
    
    onDragEnter = (event: React.DragEvent<HTMLTableRowElement>) => {
        event.stopPropagation();
        if (!this.state.isDragOver) {
            this.setState({
                isDragOver: true,
            });
        }
    };

    onDragOver = (event: React.DragEvent<HTMLTableRowElement>) => {
        event.preventDefault();
        event.stopPropagation();
    };
    onDragEnd = () => {
        this.setState({
            isDragging: false,
        });
    };

    onDragDrop = (event: React.DragEvent<HTMLTableRowElement>) => {
        event.stopPropagation();
        const id = event.currentTarget.id
        const dragElemId = event.dataTransfer.getData('text')
        const dragIndex = parseInt(dragElemId)
        
        this.props.onDrop(id, dragIndex);
        this.setState({
            isDragOver: false,
            isDragging: false,
        });
    };

    changeSelector = (id: string) => {
        const { isDragOver, isDragging } = this.state;

        if (isDragOver) {
            return "draggable dragover";
        } else if (isDragging) {
            return "draggable isdragging";
        } else {
            return "draggable";
        }
    };

    public render() {
        const { vacancy, index } = this.props;
        const { id, name, location, employer, salary, requirement } = vacancy;
        const strIndex = index.toString();
        return (
            <tr
                key={id}
                id={strIndex}
                draggable={true}
                onDragStart={this.onDragStart}
                onDragEnd={this.onDragEnd}
                onDragEnter={this.onDragEnter}
                onDragLeave={this.onDragLeave}
                onDragOver={this.onDragOver}
                onDrop={this.onDragDrop}
                className={this.changeSelector(id)}
            >
                <td className="table__column content">{name}</td>
                <td className="table__column content">{location}</td>
                <td className="table__column content">{employer}</td>
                <td className="table__column content">
                    {salary?.from && `${salary?.from} (${salary?.currency})`}
                </td>
                <td className="table__column content">{requirement}</td>
            </tr>
        );
    }
}
