import * as React from "react";
import PageItem from "./PageItem";

export interface IPagingProps {
    totalAmountPages: number;
    currentPage: number
    getNewPage: (event: React.SyntheticEvent<HTMLButtonElement>) => void;
}

export default class Paging extends React.Component<IPagingProps> {
    renderPageItems = () => {
        const { totalAmountPages, currentPage, getNewPage } = this.props;
        let pages = [];
        for (let i = 1; i <= totalAmountPages; i++) {
            let isActive = false
            if(currentPage === i) {
                isActive = true
            }
            const pageNumber = i.toString();
            const page = (
                <PageItem
                    key={i}
                    pageNumber={pageNumber}
                    getNewPage={getNewPage}
                    isActive={isActive}
                />
            );
            pages.push(page);
        }
        return pages;
    };

    public render() {
        const { getNewPage, currentPage } = this.props;
        return (
            <div>
                <ul className="pagination">
                    <li className={currentPage <= 1 ? "page-item disabled" : "page-item"}>
                        <button
                            className="page-link"
                            id="previouse"
                            onClick={getNewPage}
                        >
                            Previouse
                        </button>
                    </li>
                    {this.renderPageItems()}
                    <li className={currentPage >= 10 ? "page-item disabled" : "page-item"}>
                        <button
                            className="page-link"
                            id="next"
                            onClick={getNewPage}
                        >
                            Next
                        </button>
                    </li>
                </ul>
            </div>
        );
    }
}
