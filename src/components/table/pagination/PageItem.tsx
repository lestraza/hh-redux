import * as React from "react";

export interface IPageItemProps {
    pageNumber: string;
    isActive: boolean
    getNewPage: (event: React.SyntheticEvent<HTMLButtonElement>) => void;
}


export default class PageItem extends React.Component<IPageItemProps> {
    public render() {
        const { pageNumber, isActive, getNewPage } = this.props;
        return (
            <li className={isActive? "page-item active" : "page-item"} >
                <button className="page-link" onClick={getNewPage}
                 id={pageNumber}
                >
                {pageNumber}

                </button>
            </li>
        );
    }
}

