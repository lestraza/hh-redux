import * as React from "react";

export interface IButtonProps {
    buttonName: string
    onClick: () => void
}

export default class Button extends React.Component<IButtonProps> {

    public render() {
        const { buttonName, onClick } = this.props;
        return (
            <div className='btn btn-primary' onClick={onClick}>
                {buttonName}
            </div>
        );
    }
}
