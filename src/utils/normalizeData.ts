
import { IVacancy } from './../reducers/state.interface';


export const normalizeData = (data: IVacancy[]) => {
    return data.map(item => {
        let salary
        if(item.salary) {
           salary = {
                to: item.salary.to?.toString(),
                from: item.salary.from?.toString(),
                currency: item.salary.currency,
                gross: item.salary.gross
            };
        } else {
            salary = null
        }
        let location
        if(item.address?.city) {
            location = item.address.city
        } else {
            location = null
        }
        
        return {
            id: item.id,
            name: item.name,
            vacancyUrl: item.type.url,
            location,
            salary,
            employer: item.employer.name,
            requirement: item.snippet.requirement
        };
    });
};
