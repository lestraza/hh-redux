import { INormalaizeSalary } from "./../reducers/state.interface";
import { INormalizeData } from "./../requests/requests.interface";
import { NAME, EMPLOYER, LOCATION, REQUIRMENT, SALARY}from '../constants/constants'


const stringProps = [NAME, EMPLOYER, LOCATION, REQUIRMENT, SALARY];

const isSalary = (prop: unknown, value: unknown): value is INormalaizeSalary => {
    return typeof value === "object" && prop === "salary";
};

const isString = (prop: unknown, value: unknown): value is string => {
    return typeof value === "string" && stringProps.includes(prop as string);
};

export const filterData = (
    filterValue: string,
    prop: keyof INormalizeData,
    files: INormalizeData[],
    renderData: INormalizeData[],
    storeFilterValue: string
) => {
    let filesToFilter: INormalizeData[] = [];
    if (filterValue === storeFilterValue) {
        filesToFilter = renderData;
    } else {
        filesToFilter = files;
    }
    return filesToFilter.filter((file) => {
        const value = file[prop];
        if (value) {
            if (isSalary(prop, value)) {
                if (filterBySalary(value, filterValue)) {
                    return file;
                }
            } else if (isString(prop, value) && value.includes(filterValue)) {
                return file;
            } 
        }
    });
};

const filterBySalary = (salary: INormalaizeSalary, filterValue: string) => {
    if(salary.from?.includes(filterValue)) {
        return true;
    } else {
        return false;
    }
};
