import { INormalizeData } from './../requests/requests.interface';

export type IDispatchActions =
    | "LOAD_DATA_INFINITY"
    | "LOAD_DATA_PAIGING"
    | "FILTER_DATA"
    | "MODIFY_RENDER_DATA"

export interface IAction {
    type: IDispatchActions;
    payload: IPayload;
}

export interface IPayload {
    vacancies?: INormalizeData[];
    filteredData?: INormalizeData[];
    newPage?: number;
    filterValue?: string;
}