import { IAction } from "./action.interface";
import { INormalizeData } from "./../requests/requests.interface";

export const actionCreatorLoadDataInfinity = (
    vacancies: INormalizeData[]
): IAction =>
    <const>{
        type: "LOAD_DATA_INFINITY",
        payload: { vacancies },
    };

export const actionCreatorLoadDataPaging = (
    vacancies: INormalizeData[],
    newPage: number
): IAction =>
    <const>{
        type: "LOAD_DATA_PAIGING",
        payload: { vacancies, newPage },
    };

export const actionCreatorFilterData = (
    filteredData: INormalizeData[],
    filterValue: string
): IAction =>
    <const>{
        type: "FILTER_DATA",
        payload: { filteredData, filterValue },
    };

export const actionCreatorModifyRenderData = (vacancies: INormalizeData[]):IAction =>
    <const>{
        type: "MODIFY_RENDER_DATA",
        payload: { vacancies },
    };
