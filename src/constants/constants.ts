export const NAME: string = "name";
export const LOCATION: string = "location";
export const EMPLOYER: string = "employer";
export const REQUIRMENT: string = "requirement";
export const SALARY: string = "salary";

export const LOAD_DATA_INFINITY: string = "LOAD_DATA_INFINITY";
export const LOAD_DATA_PAGING: string = "LOAD_DATA_PAIGING";
export const FILTER_DATA: string = "FILTER_DATA";
export const MODIFY_RENDER_DATA: string = "MODIFY_RENDER_DATA"