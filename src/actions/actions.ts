import { IAction } from './../actionCreators/action.interface';
import { IAppState } from "./../reducers/state.interface";
import { filterData } from "./../utils/filterData";
import { INormalizeData } from "./../requests/requests.interface";
import { getVacanciesReq } from "../requests/requests";
import {
    actionCreatorLoadDataInfinity,
    actionCreatorLoadDataPaging,
    actionCreatorFilterData,
    actionCreatorModifyRenderData
} from "../actionCreators/actionCreators";
import { Dispatch } from "react";

export const getVacanciesAction = (pageNumber?: number) => {
    return (
        dispatch: Dispatch<IAction>,
        getState: () => IAppState
    ) => {
        let newPage: number = 0;
        const { currentPage } = getState();
        if (pageNumber === undefined) {
            newPage = currentPage + 1;
        } else {
            newPage = pageNumber;
        }
        getVacanciesReq(newPage).then((res) => {            
            if (!pageNumber) {
                dispatch(actionCreatorLoadDataInfinity(res));
            } else {
                dispatch(actionCreatorLoadDataPaging(res, newPage));
            }
        });
    };
};

export const filterDataAction = (
    filterValue: string,
    prop: keyof INormalizeData
) => {
    return (
        dispatch: Dispatch<IAction>,
        getState: () => IAppState
    ) => {
        const {
            data: files,
            renderData,
            filterValue: storeFilterValue,
        } = getState();
        const filteredData = filterData(
            filterValue,
            prop,
            files,
            renderData,
            storeFilterValue
        );
        dispatch(actionCreatorFilterData(filteredData, filterValue));
    };
};

export const modifyRenderData = (newRenderData: INormalizeData[]) => {
    return (dispatch: Dispatch<IAction>) => {
        dispatch(actionCreatorModifyRenderData(newRenderData))
    }

}
