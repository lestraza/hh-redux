import { ActionTypes } from "./../actionCreators/index";
import {
    LOAD_DATA_INFINITY,
    FILTER_DATA,
    LOAD_DATA_PAGING,
    MODIFY_RENDER_DATA,
} from "./../constants/constants";
import { IAppState } from "./state.interface";

const initialState: IAppState = {
    data: [],
    renderData: [],
    filterValue: "",
    currentPage: 0,
    totalAmountPages: 10,
};

const reducer = (state: IAppState = initialState, action: ActionTypes) => {
    switch (action.type) {
        case LOAD_DATA_INFINITY:
            return {
                ...state,
                data: [...state.data, ...action.payload.vacancies!],
                renderData: [...state.renderData, ...action.payload.vacancies!],
                currentPage: state.currentPage + 1,
            };
        case LOAD_DATA_PAGING:
            return {
                ...state,
                data: [...action.payload.vacancies!],
                renderData: [...action.payload.vacancies!],
                currentPage: action.payload.newPage,
            };
        case FILTER_DATA:
            return {
                ...state,
                renderData: action.payload.filteredData,
                filterValue: action.payload.filterValue,
            };
        case MODIFY_RENDER_DATA:
            return {
                ...state,
                renderData: [...action.payload.vacancies!],
            };

        default:
            return state;
        }
};
export default reducer;
