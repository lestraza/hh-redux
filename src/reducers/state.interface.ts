import { INormalizeData } from './../requests/requests.interface';
export interface IAppState {
    data: INormalizeData[]
    renderData: INormalizeData[]
    filterValue: string
    currentPage: number
    totalAmountPages: number
}

export interface IVacancy {
    id: string
    vacancyUrl: string
    address: IVacancyLocation | null
    name: string
    description?: string
    salary: ISalary | null
    employerId: string
    employerName: string
    requirement: string | null
    employer: IVacancyEmployer
    type: IVacancyType
    snippet: IVacancySnippets
    
}

export interface IVacancyEmployer {
    name: string
    id: string
}
export interface IVacancySnippets {
    requirement: string | null
}

export interface IVacancyType {
    url: string
}

export interface IVacancyLocation {
    city: string | null
    building: string | null
}

export interface ISalary {
    to: number| null
    from: number | null
    currency: string
    gross: boolean
}
export interface INormalaizeSalary {
    to: string | undefined
    from: string | undefined
    currency: string
    gross: boolean
}

